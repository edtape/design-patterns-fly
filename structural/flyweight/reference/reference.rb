class FlyWeight
  attr_reader :key

  def initialize(key)
    @key = key
  end
end

class FlyWeightFactory
  attr_reader :count

  def initialize
    @flyweights = {}
    @count = 0
  end

  def find(key)
    return @flyweights[key] if @flyweights.has_key?(key)

    @count += 1
    @flyweights[key] = FlyWeight.new(key)
    @flyweights[key]
  end
end

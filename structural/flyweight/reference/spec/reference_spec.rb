require_relative '../reference'

describe 'Reference implementation' do
  it 'creates a new flyweight object when it does not exist' do
    key = 'a_key'

    factory = FlyWeightFactory.new
    flyweight = factory.find(key)

    expect(flyweight.key).to eq(key)
    expect(factory.count).to eq(1)
  end

  it 'returns an existing object if it is already created' do
    key = 'a_key'
    factory = FlyWeightFactory.new
    factory.find(key)

    flyweight = factory.find(key)

    expect(flyweight.key).to eq(key)
    expect(factory.count).to eq(1)
  end
end

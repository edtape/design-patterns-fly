class CommonBall
  attr_reader :color, :radio

  def initialize(color)
    @color = color
    @radio = 4
  end
end

class BallFactory
  attr_reader :number_of_common_balls

  def initialize
    @balls = {}
    @number_of_common_balls = 0
  end

  def find(color)
    return @balls[color] if @balls.has_key?(color)

    @number_of_common_balls += 1
    @balls[color] = CommonBall.new(color)
    @balls[color]
  end
end

class Ball
  attr_accessor :common, :coordenate_x, :coordenate_y

  def initialize(color, coordenate_x, coordenate_y, factory)
    @common = factory.find(color)
    @coordenate_x = coordenate_x
    @coordenate_y = coordenate_y
  end
end

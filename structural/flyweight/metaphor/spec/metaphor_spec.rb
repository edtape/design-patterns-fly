require_relative '../metaphor'

RSpec.describe 'Metaphor implementation' do
  it 'returns a ball' do
    color = 'red'
    coordenate_x = 1
    coordenate_y = 2
    factory = BallFactory.new

    ball = Ball.new(color, coordenate_x, coordenate_y, factory)

    expect(factory.number_of_common_balls).to eq(1)
    expect(ball.common.color).to eq(color)
    expect(ball.coordenate_x).to eq(coordenate_x)
    expect(ball.coordenate_y).to eq(coordenate_y)
  end

  it 'creates new two balls with distincs colors' do
    color = 'red'
    other_color = 'blue'
    coordenate_x = 1
    coordenate_y = 2
    factory = BallFactory.new

    Ball.new(color, coordenate_x, coordenate_y, factory)
    Ball.new(other_color, coordenate_x, coordenate_y, factory)

    expect(factory.number_of_common_balls).to eq(2)
  end

  it 'returns the same ball color when already exist with different coordenates' do
    color = 'red'
    coordenate_x = 1
    coordenate_y = 2
    other_coordenate_x = 3
    other_coordenate_y = 4
    factory = BallFactory.new
    Ball.new(color, coordenate_x, coordenate_y, factory)

    ball = Ball.new(color, other_coordenate_x, other_coordenate_y, factory)

    expect(factory.number_of_common_balls).to eq(1)
    expect(ball.common.color).to eq(color)
    expect(ball.coordenate_x).to eq(other_coordenate_x)
    expect(ball.coordenate_y).to eq(other_coordenate_y)
  end
end

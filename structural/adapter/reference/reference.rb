class SomethingNotOperable
  def method
    :result
  end
end

class Operable
  def operate
  end
end

class Adapter < Operable
  def initialize(adaptee)
    @adaptee = adaptee
  end

  def operate
    @adaptee.method
  end
end

require_relative '../minimal'

describe 'Reference implementation' do
  it 'returns the value from adaptee object using the adapter' do
    class Client
      def self.execute(an_operable_thing)
        an_operable_thing.operate
      end
    end
    adapter = Adapter.new(SomethingNotOperable.new)

    result = Client.execute(adapter)

    expect(result).to eq(:result)
  end
end

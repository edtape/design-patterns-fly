class SomethingNotOperable
end

class Adapter
  def initialize(adaptee)
  end

  def operate
    :result
  end
end

class MongoClient
  def self.set(data)
    return data
  end
end

class DynamoClient
  def self.put(data)
    :stored
  end
end

class ServiceRepository
  def save
  end
end

class MongoServiceRepository < ServiceRepository
  def save(data)
    stored_data = MongoClient.set(data)
    return :stored if stored_data == data

    :failed
  end
end

class DynamoServiceRepository < ServiceRepository
  def save(data)
    DynamoClient.put(data)
  end
end

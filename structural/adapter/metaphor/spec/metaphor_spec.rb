require_relative '../metaphor'

describe 'Metaphor implementation' do
  class Service
    def initialize(repository)
      @repository = repository
    end

    def store(data)
      @repository.save(data)
    end
  end

  it 'stores the data with mongo adapter' do
    service = Service.new(MongoServiceRepository.new)

    data = {}

    expect(service.store(data)).to eq(:stored)
  end

  it 'stores the data with dynamo adapter' do
    service = Service.new(DynamoServiceRepository.new)

    data = {}

    expect(service.store(data)).to eq(:stored)
  end
end

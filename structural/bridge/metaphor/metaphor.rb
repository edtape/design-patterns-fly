# --------------Paint-----------------
#       /               \
#  PaintCar            PaintTruck
#    /    \             /      \
# CarRed CarBlue   TruckRed   TruckBlue
#
# Refactor:
#
# -------------Paint-----------------
#           /        \
# PaintCar(Color)   PaintTruck(Color)
#
#      Color
#      /   \
#     Red  Blue

class Paint
  def self.paint(color)
    raise 'must be implemented'
  end
end

class PaintCar < Paint
  def self.paint(color)
    color.paint_door
  end
end

class PaintTruck < Paint
  def self.paint(color)
    color.paint_roof
  end
end

class Color
  def paint_roof
    raise 'must be implemented'
  end

  def paint_door
    raise 'must be implemented'
  end
end

class RedColor < Color
  def paint_door
    'red twice'
  end

  def paint_roof
    'red'
  end
end

class BlueColor < Color
  def paint_door
    'blue twice'
  end

  def paint_roof
    'blue'
  end
end

class Client
  red = RedColor.new
  blue = BlueColor.new

  PaintCar.paint(red)
  PaintCar.paint(blue)

  PaintTruck.paint(red)
  PaintTruck.paint(blue)
end

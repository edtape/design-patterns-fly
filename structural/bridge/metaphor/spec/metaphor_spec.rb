require_relative '../metaphor'

describe 'Metaphor implementation' do
  it 'can paint the car in red' do
    red = RedColor.new

    painted_color = PaintCar.paint(red)

    expect('red twice').to eq(painted_color)
  end

  it 'can paint the car in blue' do
    blue = BlueColor.new

    painted_color = PaintCar.paint(blue)

    expect('blue twice').to eq(painted_color)
  end

  it 'can paint the truck in red' do
    red = RedColor.new

    painted_color = PaintTruck.paint(red)

    expect('red').to eq(painted_color)
  end

  it 'can paint the truck in blue' do
    blue = BlueColor.new

    painted_color = PaintTruck.paint(blue)

    expect('blue').to eq(painted_color)
  end
end

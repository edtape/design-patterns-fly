class ConcreteImplementorOne
  def message
    "something one_thing"
  end
end

class ConcreteImplementorTwo
  def message
    "something second_thing"
  end
end

class RefinedAbstraction
  def self.print_message_for(implementor)
    implementor.message
  end
end

require_relative '../minimal'

describe 'Reference implementation' do
  it 'print concrete message for a ConcreteImplementorOne' do
    implementor = ConcreteImplementorOne.new

    message = RefinedAbstraction.print_message_for(implementor)

    expect(message).to eq('something one_thing')
  end

  it 'print concrete message for a ConcreteImplementorTwo' do
    implementor = ConcreteImplementorTwo.new

    message = RefinedAbstraction.print_message_for(implementor)

    expect(message).to eq('something second_thing')
  end
end

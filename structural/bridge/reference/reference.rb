class Abstraction
  def self.print_message_for(implementor)
    raise 'must be implemented'
  end
end

class RefinedAbstraction < Abstraction
  def self.print_message_for(implementor)
    'something ' + implementor.message
  end
end

class Implementor
  def message
    raise 'must be implemented'
  end
end

class ConcreteImplementorOne < Implementor
  def message
    'one_thing'
  end
end

class ConcreteImplementorTwo < Implementor
  def message
    'second_thing'
  end
end

class Client
  implementor_one = ConcreteImplementorOne.new
  RefinedAbstraction.print_message_for(implementor_one)

  implementor_two = ConcreteImplementorTwo.new
  RefinedAbstraction.print_message_for(implementor_two)
end

class IceCream
  def description
    raise 'method must be implemented'
  end
end

class SimpleIceCream < IceCream
  def description
    'One ball of ice cream'
  end
end

class IceCreamDecorator < IceCream
end

class TwoBallIceCreamDecorator < IceCreamDecorator
  def initialize(ice_cream)
    @ice_cream = ice_cream
  end

  def description
    @ice_cream.description + ', with an extra ball'
  end
end

class BiscuitIceCreamDecorator < IceCreamDecorator
  def initialize(ice_cream)
    @ice_cream = ice_cream
  end

  def description
    @ice_cream.description + ', with biscuits'
  end
end

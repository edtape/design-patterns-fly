require_relative '../metaphor'

RSpec.describe 'Metaphor implementation' do
  it 'return the price of the simple ice cream' do
    ice_cream = SimpleIceCream.new

    expect(ice_cream.description).to eq('One ball of ice cream')
  end

  it 'return the price of the two ball ice cream' do
    ice_cream = SimpleIceCream.new
    two_ball_ice_cream = TwoBallIceCream.new(ice_cream)

    expect(two_ball_ice_cream.description).to eq('One ball of ice cream, with an extra ball')
  end

  it 'return the price of the biscuit ice cream' do
    ice_cream = SimpleIceCream.new
    biscuit_ice_cream = BiscuitIceCream.new(ice_cream)

    expect(two_ball_ice_cream.description).to eq('One ball of ice cream, with biscuits')
  end

  it 'return the price of the two ball ice cream with biscuits' do
    ice_cream = SimpleIceCream.new
    two_ball_ice_cream = TwoBallIceCream.new(ice_cream)
    biscuit_ice_cream = BiscuitIceCream.new(two_ball_ice_cream)

    expect(biscuit_ice_cream.description).to eq('One ball of ice cream, with an extra ball, with biscuits')
  end
end

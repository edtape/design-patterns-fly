class Component
  def operation
    raise 'must be implemented'
  end
end

class ConcreteComponent < Component
  def operation
    1
  end
end

class Decorator < Component
end

class ConcreteDecorator < Decorator
  def initialize(component)
    @component = component
  end

  def operation
    @component.operation + 2
  end
end


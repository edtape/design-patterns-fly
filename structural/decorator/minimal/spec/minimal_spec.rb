require_relative '../minimal'

describe 'Minimal implementation' do
  it 'returns the result operation for a concrete component' do
    concrete_component = ConcreteComponent.new
    expect(concrete_component.operation).to eq(1)
  end

  it 'returns the result operation for a concrete decorator' do
    concrete_component = ConcreteComponent.new
    concrete_decorator = ConcreteDecorator.new(concrete_component)
    expect(concrete_decorator.operation).to eq(3)
  end
end

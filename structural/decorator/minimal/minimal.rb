class ConcreteComponent
  def operation
    1
  end
end

class ConcreteDecorator
  def initialize(component)
    @component = component
  end

  def operation
    @component.operation + 2
  end
end


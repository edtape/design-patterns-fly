require_relative '../metaphor'

describe 'Metaphor implementation' do
  it 'takes two pens from the box' do
    box = PenBox.new

    pen_one = box.take_a_pen
    pen_two = box.take_a_pen

    expect(pen_one.id).to eq(1)
    expect(pen_two.id).to eq(2)
  end

  it 'takes a pen already used when someone take a pen and other persone leave other pen' do
    box = PenBox.new

    box.take_a_pen
    pen_two = box.take_a_pen
    box.leave_a(pen_two)
    pen_three = box.take_a_pen

    expect(pen_three.id).to eq(2)
  end

  it 'returns a nil reporter when clients reach the limit of reporters' do
    box = PenBox.new
    box.set_max_pens(2)

    box.take_a_pen
    box.take_a_pen
    no_more_pens = box.take_a_pen

    expect(no_more_pens).to be(nil)
  end
end

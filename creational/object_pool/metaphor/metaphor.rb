class Pen
  attr_reader :id

  def initialize(id)
    @id = id
  end

  def draw
    :draw
  end
end

class PenBox
  DEFAULT_MAX_PENS = 5

  def initialize
    @used = []
    @free = []
    @max_pens = DEFAULT_MAX_PENS
  end

  def take_a_pen
    return nil if @used.size >= @max_pens
    return obtain_free_pen unless @free.empty?

    create_pen
  end

  def leave_a(pen)
    @used.delete(pen)
    @free.push(pen)
  end

  def set_max_pens(limit)
    @max_pens = limit
  end

  private

  def obtain_free_pen
    pen = @free.first
    @free.delete(pen)
    @used.push(pen)

    pen
  end

  def create_pen
    pen = Pen.new(generate_id)
    @used.push(pen)

    pen
  end

  def generate_id
    return 1 if @used.empty?

    @used.size + 1
  end
end

box = PenBox.new

pen_one = box.take_a_pen
pen_two = box.take_a_pen

pen_one.draw
pen_two.draw

box.leave_a(pen_one)
box.leave_a(pen_two)

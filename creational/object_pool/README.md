## Object Pool

### Definition

The object pool design pattern creates a set of objects that may be reused.

The object pool pattern is a software creational design pattern that uses a set of initialized objects kept ready to use rather than allocating and destroying them on demand.

A client of the pool will request an object from the pool and perform operations on the returned object. When the client has finished, it returns the object to the pool rather than destroying it; this can be done manually or automatically.

Object pools are primarily used for performance: in some circumstances, object pools significantly improve performance. Object pools complicate object lifetime, as objects obtained from and returned to a pool are not actually created or destroyed at this time, and thus require care in implementation.

### Usage

When a new object is needed, it is requested from the pool. If a previously prepared object is available it is returned immediately, avoiding the instantiation cost. If no objects are present in the pool, a new item is created and returned. When the object has been used and is no longer needed, it is returned to the pool, allowing it to be used again in the future without repeating the computationally expensive instantiation process. It is important to note that once an object has been used and returned, existing references will become invalid.

In some object pools the resources are limited so a maximum number of objects is specified. If this number is reached and a new item is requested, an exception may be thrown, or the thread will be blocked until an object is released back into the pool.

### Metaphor implementation

For this pattern I used a problem about how to generate a pool to generate reporters. So when a client wants to generate 5 different reports the pool returns a reporter object that generate a report. If a client ask for more than 5 reports the pool return nothing, in this case we could throw an exception. When the report is generated the reporter object is released and could be reused.


class ReusableObject
  attr_reader :id

  def initialize(id)
    @id = id
  end
end

class ReusablePool
  DEFAULT_LIMIT_SIZE = 5

  def initialize
    @used_objects = []
    @free_objects = []
    @max_size = DEFAULT_LIMIT_SIZE
  end

  def acquire
    return nil if @used_objects.size >= @max_size
    return obtain_reusable_object unless @free_objects.empty?

    create_reusable_object
  end

  def release(reusable_object)
    @free_objects.push(reusable_object)
    @used_objects.delete(reusable_object)
  end

  def set_max_pool_size(limit)
    @max_size = limit
  end

  private

  def obtain_reusable_object
    reusable_object = @free_objects.first
    @free_objects.delete(reusable_object)
    @used_objects.push(reusable_object)

    reusable_object
  end

  def create_reusable_object
    reusable_object = ReusableObject.new(reusable_id)
    @used_objects.push(reusable_object)
    reusable_object
  end

  def reusable_id
    return 1 if @used_objects.empty?

    @used_objects.size + 1
  end
end

require_relative '../minimal'

describe 'Reference implementation' do
  it 'creates two reusable objects when a client acquire a reusable object from pool' do
    manager = ReusablePool.new

    reusable_one = manager.acquire
    reusable_two = manager.acquire

    expect(reusable_one.id).to eq(1)
    expect(reusable_two.id).to eq(2)
  end

  it 'use a free reusable object when a client acquire two then release one and acquire another one' do
    manager = ReusablePool.new

    manager.acquire
    reusable_two = manager.acquire
    manager.release(reusable_two)
    reusable_three = manager.acquire

    expect(reusable_three.id).to eq(2)
  end

  it 'return nil when a client reach the limit of the pool' do
    manager = ReusablePool.new
    manager.set_max_pool_size(2)

    manager.acquire
    manager.acquire
    reusable_object_limit_reached = manager.acquire

    expect(reusable_object_limit_reached).to be(nil)
  end
end

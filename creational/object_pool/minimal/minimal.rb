class ReusableObject
  attr_reader :id

  def initialize(id)
    @id = id
  end
end

class ReusablePool
  DEFAULT_LIMIT_SIZE = 5

  def initialize
    @used_objects = []
    @free_objects = []
    @max_size = DEFAULT_LIMIT_SIZE
  end

  def acquire
    return nil if @used_objects.size >= @max_size

    unless @free_objects.empty?
      reusable_object = @free_objects.first
      @free_objects.delete(reusable_object)
      @used_objects.push(reusable_object)
      return reusable_object
    end

    reusable_object = ReusableObject.new(reusable_id)
    @used_objects.push(reusable_object)
    reusable_object
  end

  def release(reusable_object)
    @used_objects.delete(reusable_object)
    @free_objects.push(reusable_object)
  end

  def set_max_pool_size(limit)
    @max_size = limit
  end

  private

  def reusable_id
    return 1 if @used_objects.empty?

    @used_objects.size + 1
  end
end

require_relative '../metaphor'

describe 'Metaphor implementation' do
  it 'renders a csv file when client provide a file' do
    generator = GenerateCsv.new

    generated_file = generator.create

    expect(generated_file.render).to eq(:file_one)
  end

  it 'renders a xml file when client provide a file' do
    generator = GenerateXml.new

    generated_file = generator.create

    expect(generated_file.render).to eq(:file_two)
  end
end

class Generator
  def create
    raise 'Must be implemented'
  end
end

class GenerateCsv < Generator
  def create
    CsvFile.new
  end
end

class GenerateXml < Generator
  def create
    XmlFile.new
  end
end

class UniversalFile
  def render
    raise 'Must be implemented'
  end

  def write(content)
    raise 'Must be implemented'
  end
end

class CsvFile < UniversalFile
  def render
   :file_one
  end

  def write(content)
    :write_content
  end
end

class XmlFile < UniversalFile
  def render
   :file_two
  end

  def write(content)
    :write_content
  end
end

class Client
  def initialize(generator)
    @generator = generator
  end

  def print_file_with(content)
    file = @generator.create
    file.write(content)
    file.render
  end
end

client = Client.new(GenerateCsv.new)
client.print_file_with(:content)

client = Client.new(GenerateXml.new)
client.print_file_with(:content)

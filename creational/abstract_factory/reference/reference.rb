class AbstractFactory
  def create
    raise 'You must implement this method'
  end
end

class ConcreteFactoryOne < AbstractFactory
  def create
    ConcreteProductOne.new
  end
end

class ConcreteFactoryTwo < AbstractFactory
  def create
    ConcreteProductTwo.new
  end
end

class AbstractProduct
  def obtain
    raise 'You must implement this method'
  end
end

class ConcreteProductOne < AbstractProduct
  def obtain
    :one
  end
end

class ConcreteProductTwo < AbstractProduct
  def obtain
    :two
  end
end

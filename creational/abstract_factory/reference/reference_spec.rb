require_relative '../reference'

describe 'Reference implementation' do
  it 'returns the products one when a client use ConcreteFactoryOne' do
    factory = ConcreteFactoryOne.new

    product = factory.create

    expect(product.obtain).to eq(:one)
  end

  it 'returns the products two when a client use ConcreteFactoryTwo' do
    factory = ConcreteFactoryTwo.new

    product = factory.create

    expect(product.obtain).to eq(:two)
  end
end

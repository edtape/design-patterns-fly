class ConcreteFactoryOne
  def create
    ConcreteProductOne.new
  end
end

class ConcreteFactoryTwo
  def create
    ConcreteProductTwo.new
  end
end

class ConcreteProductOne
  def obtain
    :one
  end
end

class ConcreteProductTwo
  def obtain
    :two
  end
end


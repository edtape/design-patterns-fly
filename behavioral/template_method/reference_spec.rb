require_relative 'reference'

describe 'Reference implementation' do
  it 'should implement step_one' do
    class ATemplateClass < TemplateClass
      def step_one
        :something_concrete
      end
    end
    a_template_class = ATemplateClass.new

    result = a_template_class.template_method

    expect(result).to eq('something_concrete step_two')
  end

  it 'should implement step_two' do
    class OtherTemplateClass < TemplateClass
      def step_two
        :something_concrete
      end
    end
    a_template_class = OtherTemplateClass.new

    result = a_template_class.template_method

    expect(result).to eq('step_one something_concrete')
  end
end

require_relative 'metaphor'

describe 'Metaphor implementation' do
  it 'makes a chocolate cake' do
    chocolate_cake = ChocolateCake.new

    result = chocolate_cake.make_a_cake

    expect(result).to eq('common_ingredients chocolate cook_at_200_degrees')
  end

  it 'makes a apple cake' do
    apple_cake = AppleCake.new

    result = apple_cake.make_a_cake

    expect(result).to eq('common_ingredients apple cook_at_180_degrees')
  end
end

class Cake
  def make_a_cake
    "#{common_ingredients} #{specific_ingredients} #{temperature_in_oven}"
  end

  protected

  def common_ingredients
    :common_ingredients
  end

  def add_ingredients
  end

  def temperature_in_oven
  end
end

class ChocolateCake < Cake
  protected

  def specific_ingredients
    :chocolate
  end

  def temperature_in_oven
    :cook_at_200_degrees
  end
end

class AppleCake < Cake
  protected

  def specific_ingredients
    :apple
  end

  def temperature_in_oven
    :cook_at_180_degrees
  end
end

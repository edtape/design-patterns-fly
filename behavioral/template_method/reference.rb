class TemplateClass
  def template_method
    "#{step_one} #{step_two}"
  end

  def step_one
    :step_one
  end

  def step_two
    :step_two
  end
end

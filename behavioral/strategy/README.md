# Description
The strategy pattern, also known as the policy pattern, is a behavioral software design pattern that enables selecting an algorithm at runtime.
Instead of implementing a single algorithm directly, code receives run-time instructions as to which in a family of algorithms to use.

Strategy defines a set of algorithms that can be used interchangeably by a client.

Deferring the decision about which algorithm to use until runtime allows the calling code to be more flexible and reusable.

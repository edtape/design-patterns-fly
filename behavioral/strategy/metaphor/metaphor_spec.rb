require_relative 'metaphor'

describe 'Metaphor implementation' do
  it 'when we are winning the match be conservative' do
    strategy  = WinningMatch.new

    selected_formation = Match.formation_for(strategy)

    expect(selected_formation).to eq('4-5-1')
  end

  it 'when we are losing the match go to the attack' do
    strategy  = LosingMatch.new

    selected_formation = Match.formation_for(strategy)

    expect(selected_formation).to eq('3-5-2')
  end
end

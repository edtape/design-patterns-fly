class MatchFormation
  def select
    raise 'You must implement this method'
  end
end

class WinningMatch < MatchFormation
  def select
    '4-5-1'
  end
end

class LosingMatch < MatchFormation
  def select
    '3-5-2'
  end
end

class Match
  def self.formation_for(match)
    match.select
  end
end

class Strategy
  def execute
    raise 'You must implement this method'
  end
end

class StrategyOne < Strategy
  def execute
    :one
  end
end

class StrategyTwo < Strategy
  def execute
    :two
  end
end

class Context
  def self.execute_strategy_for(strategy)
    strategy.execute
  end
end

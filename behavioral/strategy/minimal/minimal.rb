class StrategyOne
  def execute
    :one
  end
end

class StrategyTwo
  def execute
    :two
  end
end

class Context
  def self.execute_strategy_for(strategy)
    strategy.execute
  end
end

require_relative 'minimal'

describe 'Minimal implementation' do
  it 'returns one when the context use StrategyOne' do
    strategy = StrategyOne.new

    result = Context.execute_strategy_for(strategy)

    expect(result).to eq(:one)
  end

  it 'returns two when the context use StrategyTwo' do
    strategy = StrategyTwo.new

    result = Context.execute_strategy_for(strategy)

    expect(result).to eq(:two)
  end
end

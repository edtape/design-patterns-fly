require_relative 'metaphor'

describe 'Metaphor implementation' do
  it 'passes when light is green' do
    light = GreenLight.new
    semaphor = Semaphor.new(light)

    result = semaphor.do

    expect(result).to eq(:pass)
  end

  it 'stop when light is red' do
    light = RedLight.new
    semaphor = Semaphor.new(light)

    result = semaphor.do

    expect(result).to eq(:stop)
  end

  it 'accelerate when light is yellow' do
    light = YellowLight.new
    semaphor = Semaphor.new(light)

    result = semaphor.do

    expect(result).to eq(:accelerate)
  end
end

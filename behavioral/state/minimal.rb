class Context
  def initialize(state)
    @state = state
  end

  def request
    @state.execute
  end
end

class StateOne
  def execute
    :state_one
  end
end

class StateTwo
  def execute
    :state_two
  end
end


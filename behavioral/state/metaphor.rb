class Semaphor
  attr_reader :light

  def initialize(light)
    @light = light
  end

  def do
    @light.do
  end
end

class Light
  def do
    raise 'Method must be implemented'
  end
end

class RedLight < Light
  def do
    :stop
  end
end

class YellowLight < Light
  def do
    :accelerate
  end
end

class GreenLight < Light
  def do
    :pass
  end
end

require_relative 'reference'

describe 'Reference implementation' do
  it 'return state_one when state is StateOne' do
    state= StateOne.new
    context = Context.new(state)

    result = context.request

    expect(result).to eq(:state_one)
  end

  it 'return state_two when state is StateTwo' do
    state = StateTwo.new
    context = Context.new(state)

    result = context.request

    expect(result).to eq(:state_two)
  end
end

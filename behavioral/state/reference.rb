class Context
  attr_reader :state

  def initialize(state)
    @state = state
  end

  def request
    @state.execute
  end
end

class State
  def execute
    raise 'Method must be implemented'
  end
end

class StateOne < State
  def execute
    :state_one
  end
end

class StateTwo < State
  def execute
    :state_two
  end
end

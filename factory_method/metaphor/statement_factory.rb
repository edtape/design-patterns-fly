require_relative 'statements/csv_statement'
require_relative 'statements/bai2_statement'
require_relative 'statements/xml_statement'

class StatementFactory
  STATEMENT_FOR = {
    csv: CsvStatement,
    bai2: Bai2Statement,
    xml: XmlStatement
  }

  def self.new(attributes)
    type = attributes[:type].to_sym
    STATEMENT_FOR[type].new(attributes)
  end
end

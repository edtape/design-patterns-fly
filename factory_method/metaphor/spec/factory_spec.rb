require_relative '../statement_factory'

describe 'Factory method' do
  context 'for a csv type' do
    it 'creates a csv statement object' do
      result = StatementFactory.new(attributes)
      expect(result.class).to be(CsvStatement)
    end

    it 'creates a statement with attributes' do
      result = StatementFactory.new(attributes).build
      expect(result).to include('a_csv_account')
    end

    it 'creates a statement with headers' do
      result = StatementFactory.new(attributes).build
      expect(result).to include('amount, account, bank')
    end

    def attributes
      {
        type: 'csv',
        amount: 200.00,
        account: 'a_csv_account',
        bank: 'a_csv_bank'
      }
    end
  end

  context 'for a bai2 type' do
    it 'creates a bai2 statement object' do
      result = StatementFactory.new(attributes)
      expect(result.class).to be(Bai2Statement)
    end

    it 'creates a statement with attributes' do
      result = StatementFactory.new(attributes).build
      expect(result).to include('a_bai2_account')
    end

    it 'creates a statement with row indicators' do
      result = StatementFactory.new(attributes).build
      expect(result).to include('01')
    end

    def attributes
      {
        type: 'bai2',
        amount: 300.00,
        account: 'a_bai2_account',
        bank: 'a_bai2_bank'
      }
    end
  end
  context 'for a xml type' do
    it 'creates a xml statement object' do
      result = StatementFactory.new(attributes)
      expect(result.class).to be(XmlStatement)
    end

    it 'creates a statements with attributes' do
      result = StatementFactory.new(attributes).build
      expect(result).to include('a_xml_account')
    end

    it 'creates a statement with labels' do
      result = StatementFactory.new(attributes).build
      expect(result).to include('<Acct>')
    end
  end

  def attributes
    {
      type: 'xml',
      amount: 300.00,
      account: 'a_xml_account',
      bank: 'a_xml_bank'
    }
  end
end
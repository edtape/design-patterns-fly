class XmlStatement
  def initialize(attributes)
    @attributes = attributes
  end

  def build
    "<Amt>#{@attributes[:amount]}</Amt><Acct>#{@attributes[:account]}</Acct><Src>#{@attributes[:bank]}</Src>"
  end
end
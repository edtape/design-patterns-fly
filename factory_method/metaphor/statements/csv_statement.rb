class CsvStatement
  def initialize(attributes)
    @attributes = attributes
  end

  def build
    "amount, account, bank\n#{@attributes[:amount]},#{@attributes[:account]}, #{@attributes[:bank]}"
  end
end
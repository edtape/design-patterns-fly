class Bai2Statement
  def initialize(attributes)
    @attributes = attributes
  end

  def build
    "01,,,,,,,,/\n02,#{@attributes[:amount]},#{@attributes[:account]}, #{@attributes[:bank]}/\n
    98,,,/\n99,,,/"
  end
end
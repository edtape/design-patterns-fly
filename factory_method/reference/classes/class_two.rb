class ClassTwo
  def initialize(attributes)
    @attributes = attributes
  end

  def do
    @attributes[:string]
  end
end
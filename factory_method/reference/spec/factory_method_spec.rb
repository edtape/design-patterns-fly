require_relative '../factory_method'

describe 'Factory method' do
  context 'for class one' do
    it 'creates a class one object' do
      result = FactoryMethod.create(class_one_attributes)
      expect(result).to be_kind_of(ClassOne)
    end

    it 'uses the class one method' do
      result = FactoryMethod.create(class_one_attributes).do
      expect(result).to eq('type one')
    end
  end

  context 'for class two' do
    it 'creates a class two object' do
      result = FactoryMethod.create(class_two_attributes)
      expect(result).to be_kind_of(ClassTwo)
    end

    it 'uses the class two method' do
      result = FactoryMethod.create(class_two_attributes).do
      expect(result).to eq('type two')
    end
  end

  def class_one_attributes
    { type: 'one', string: 'type one'}
  end

  def class_two_attributes
    { type: 'two', string: 'type two'}
  end
end
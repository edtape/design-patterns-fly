require_relative 'classes/class_one'
require_relative 'classes/class_two'

class FactoryMethod
  class << self
    def create(attributes)
      klass = select_class_for(attributes)

      generate_object_for(klass, attributes)
    end

    private

    def select_class_for(attributes)
      type = attributes[:type].to_sym

      class_catalog.fetch(type)
    end

    def generate_object_for(klass, attributes)
      klass.new(attributes)
    end

    def class_catalog
      {
        one: ClassOne,
        two: ClassTwo
      }
    end
  end
end

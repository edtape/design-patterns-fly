class FactoryMethod

  def self.build(type, attributes)
    if type == :type_one
      ClassOne.new(attributes)
    else
      ClassTwo.new(attributes)
    end
  end
end

class ClassOne
  def initialize(attributes)
    @attributes = attributes
  end

  def do
    @attributes + ' ' + 'type_one'
  end
end

class ClassTwo
  def initialize(attributes)
    @attributes = attributes
  end

  def do
    @attributes + ' ' + 'type_two'
  end
end
require_relative '../factory_method'

describe 'Factory method' do
  context 'for class one' do
    it 'creates a class one object' do
      result = FactoryMethod.build(:type_one, attributes)
      expect(result.class).to be(ClassOne)
    end

    it 'uses the class one method' do
      result = FactoryMethod.build(:type_one, attributes).do
      expect(result).to eq('this is a type_one')
    end
  end

  context 'for class two' do
    it 'creates a class two object' do
      result = FactoryMethod.build(:type_two, attributes)
      expect(result.class).to eq(ClassTwo)
    end

    it 'uses the class two method' do
      result = FactoryMethod.build(:type_two, attributes).do
      expect(result).to eq('this is a type_two')
    end
  end

  def attributes
    'this is a'
  end
end